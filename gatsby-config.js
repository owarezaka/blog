module.exports = {
	pathPrefix: `/gatsby/`,
	siteMetadata: {
		title: `Owarezaka`,
		description: `Yet Another GitLab Mobile Client`,
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/images`,
			},
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `Owarezaka`,
				start_url: `/`,
				background_color: `#663399`,
				theme_color: `#663399`,
				display: `minimal-ui`,
				icon: "./src/images/owarezaka_icon.png",
			},
		},
	],
};

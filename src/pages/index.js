import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";

const IndexPage = () => (
	<Layout>
		<h1>Hi people</h1>
		<p>Welcome to our Blog.</p>
		<div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}></div>
		<Link to='/page-2/'></Link>
	</Layout>
);

export default IndexPage;
